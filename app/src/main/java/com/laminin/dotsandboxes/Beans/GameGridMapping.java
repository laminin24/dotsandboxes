package com.laminin.dotsandboxes.Beans;

import com.laminin.dotsandboxes.Utils.GameConstants;

/**
 * Created by franklinf on 19-08-2017.
 *
 * Once the first user clicks the grid we need to send the details to second user in On line mode,
 * We can relay on the co ordinates, since they may vary based on the devices.
 * So lets name all the lines and will draw lines based on the names.
 */

public class GameGridMapping {
    public int getSourceRowNumber() {
        return sourceRowNumber;
    }

    public void setSourceRowNumber(int sourceRowNumber) {
        this.sourceRowNumber = sourceRowNumber;
    }

    public int getSourceColumnNumber() {
        return sourceColumnNumber;
    }

    public void setSourceColumnNumber(int sourceColumnNumber) {
        this.sourceColumnNumber = sourceColumnNumber;
    }

    public int getDestinationRowNumber() {
        return destinationRowNumber;
    }

    public void setDestinationRowNumber(int destinationRowNumber) {
        this.destinationRowNumber = destinationRowNumber;
    }

    public int getDestinationColumnNumber() {
        return destinationColumnNumber;
    }

    public void setDestinationColumnNumber(int destinationColumnNumber) {
        this.destinationColumnNumber = destinationColumnNumber;
    }

    int sourceRowNumber = -1;
    int sourceColumnNumber = -1;
    int destinationRowNumber = -1;
    int destinationColumnNumber = -1;
}
