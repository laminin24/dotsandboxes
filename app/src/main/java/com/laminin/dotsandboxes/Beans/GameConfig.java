package com.laminin.dotsandboxes.Beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.laminin.dotsandboxes.Utils.GameConstants;

/**
 * Created by franklin.michael on 08/08/16.
 */

public class GameConfig implements Parcelable {

    private int gameMode;
    private String playerOneName;
    private String playerTwoName;
    private int gridSize;

    protected GameConfig(Parcel in) {
        gameMode = in.readInt();
        playerOneName = in.readString();
        playerTwoName = in.readString();
        gridSize = in.readInt();
    }

    public static final Creator<GameConfig> CREATOR = new Creator<GameConfig>() {
        @Override
        public GameConfig createFromParcel(Parcel in) {
            return new GameConfig(in);
        }

        @Override
        public GameConfig[] newArray(int size) {
            return new GameConfig[size];
        }
    };

    public GameConfig(){
        gameMode = GameConstants.SAME_DEVICE_MODE;
        playerOneName = "Player 1";
        playerTwoName = "Player 2";
        gridSize = GameConstants.SIX_INTO_SIX;
    }

    public int getGameMode() {
        return gameMode;
    }

    public void setGameMode(int gameMode) {
        this.gameMode = gameMode;
    }

    public String getPlayerOneName() {
        return playerOneName;
    }

    public void setPlayerOneName(String playerOneName) {
        this.playerOneName = playerOneName;
    }

    public String getPlayerTwoName() {
        return playerTwoName;
    }

    public void setPlayerTwoName(String playerTwoName) {
        this.playerTwoName = playerTwoName;
    }

    public int getGridSize() {
        return gridSize;
    }

    public void setGridSize(int gridSize) {
        this.gridSize = gridSize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(gameMode);
        parcel.writeString(playerOneName);
        parcel.writeString(playerTwoName);
        parcel.writeInt(gridSize);
    }
}
