package com.laminin.dotsandboxes.Widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.laminin.dotsandboxes.Utils.GameConstants;
import com.laminin.dotsandboxes.Utils.GameUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by franklin.michael on 30/07/16.
 * Grid view just draw the background dots.
 * dots are filled in equal distance
 */

public class GridView extends View {

    private String TAG = "franklin";

    // screen maximum width
    private int screenWidth;
    // screen maximum height
    private int screenHeight;

    //  default circle paint
    private Paint dotCirclePaint;

    // grid point x values set
    private LinkedHashSet<Integer> gridXValesSet;
    // grid point y values set
    private LinkedHashSet<Integer> gridYValuesSet;

    // total possible rectangles.
    private int totalRectCount;

    // distance between grid lines
    private int distanceBetweenGridLines;
    // dot circle radius
    private int dotCircleRadius;
    // grid size given by the player
    private int mGridSize;
    // Game utils
    private GameUtils mGameUtils;
    // Grid start point
    private Rect mGridBoundaryRect;

    public GridView(Context context) {
        super(context);
        init();
    }

    public GridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d(TAG, "onDraw() called with: canvas = [" + canvas + "]");

        Point dotCirclePosition = new Point(mGridBoundaryRect.left, mGridBoundaryRect.top);
        for(int currentY = mGridBoundaryRect.top; currentY <= mGridBoundaryRect.bottom; currentY += distanceBetweenGridLines){
            for(int currentX = mGridBoundaryRect.left; currentX <= mGridBoundaryRect.right; currentX += distanceBetweenGridLines){
                canvas.drawCircle(dotCirclePosition.x, dotCirclePosition.y, dotCircleRadius, dotCirclePaint);
                gridXValesSet.add(dotCirclePosition.x);
                gridYValuesSet.add(dotCirclePosition.y);
                dotCirclePosition.x += distanceBetweenGridLines;
            }
            dotCirclePosition.x = mGridBoundaryRect.left;
            dotCirclePosition.y += distanceBetweenGridLines;
        }

        totalRectCount = (gridXValesSet.size() - 1) * (gridYValuesSet.size() -1);

        // Remove later
//        Paint paint = new Paint();
//        paint.setStrokeWidth(3);
//        paint.setColor(Color.BLACK);
//        paint.setStyle(Paint.Style.STROKE);
//        canvas.drawRect(mGridBoundaryRect, paint);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.d(TAG, "onSizeChanged() called with: w = [" + w + "], h = [" + h + "], oldw = [" + oldw + "], oldh = [" + oldh + "]");
        this.screenWidth = w;
        this.screenHeight = h;

        // we have screen width, height, and grid size, lets draw grid,
        distanceBetweenGridLines = screenWidth / (mGridSize + 1); // one for one more edge, so the vertices count will remain as grid size
        dotCircleRadius = distanceBetweenGridLines / GameConstants.DOT_CIRCLE_RADIUS_RATIO;
        mGridBoundaryRect = mGameUtils.getGridBoundaryRect(mGridSize, distanceBetweenGridLines);
    }

    private void init(){
        Log.d(TAG, "init() called");

        gridXValesSet = new LinkedHashSet<>();
        gridYValuesSet = new LinkedHashSet<>();

        dotCirclePaint = new Paint();
        dotCirclePaint.setColor(GameConstants.DOT_CIRCLE_COLOR);
        dotCirclePaint.setStyle(Paint.Style.FILL);

        totalRectCount = 0;

        mGameUtils = new GameUtils();
        mGridBoundaryRect = new Rect(0, 0, 0, 0);
    }

    public LinkedHashSet<Integer> getGridXValesSet(){
        return gridXValesSet;
    }

    public LinkedHashSet<Integer> getGridYValuesSet(){
        return gridYValuesSet;
    }

    public int getTotalRectCount() {
        return totalRectCount;
    }


    public void drawGrid(int gridSize){
        mGridSize = gridSize;
        invalidate();
    }
}
