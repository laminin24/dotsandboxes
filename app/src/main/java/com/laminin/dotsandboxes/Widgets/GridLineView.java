package com.laminin.dotsandboxes.Widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.laminin.dotsandboxes.Utils.GameConstants;
import com.laminin.dotsandboxes.Utils.GameUtils;

/**
 * Gird line view the default grids
 * drawn with {@link GameConstants#DISTANCE_BETWEEN_GRID_LINES} distance.
 * It should keep change it's color based on the player's turn
 * Created by franklin.michael on 30/07/16.
 */

public class GridLineView extends View {

    // screen maximum width
    private int screenWidth;
    // screen maximum height
    private int screenHeight;

    // default grid line paint
    private Paint gridLinePaint;

    // distance between grid lines
    private int distanceBetweenGridLines;
    // grid start point
    private int gridStartX;
    // grid end point.
    private int gridStartY;

    //grid size
    private int mGridSize;
    // Game utils
    private GameUtils mGameUtils;
    // Rect boundary
    private Rect mGridBoundaryRect;


    public GridLineView(Context context) {
        super(context);
        init();
    }

    public GridLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GridLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        /*gridLinePoint.set(gridStartPosition.x, gridStartPosition.y);

        // Drawing horizontal dotted lines.
        for(int currentY = gridLinePoint.y; currentY <= mGridEndPoint.y; currentY += distanceBetweenGridLines) {
            canvas.drawLine(gridLinePoint.x, gridLinePoint.y, mGridEndPoint.x, gridLinePoint.y, gridLinePaint);
            gridLinePoint.y += distanceBetweenGridLines;
        }
        gridLinePoint.set(gridStartPosition.x, gridStartPosition.y);

        // Drawing vertical dotted lines.
        for(int currentX = gridLinePoint.x; currentX <= mGridEndPoint.x; currentX += distanceBetweenGridLines) {
            canvas.drawLine(gridLinePoint.x, gridLinePoint.y, gridLinePoint.x, mGridEndPoint.y, gridLinePaint);
            gridLinePoint.x += distanceBetweenGridLines;
        }*/

        // default dotted line position
        Point gridLinePoint = new Point(mGridBoundaryRect.left, mGridBoundaryRect.top);
        // Drawing horizontal dotted lines.
        for(int currentY = gridLinePoint.y; currentY <= mGridBoundaryRect.bottom; currentY += distanceBetweenGridLines) {
            canvas.drawLine(gridLinePoint.x, gridLinePoint.y, mGridBoundaryRect.right, gridLinePoint.y, gridLinePaint);
            gridLinePoint.y += distanceBetweenGridLines;
        }
        gridLinePoint.set(mGridBoundaryRect.left, mGridBoundaryRect.top);

        // Drawing vertical dotted lines.
        for(int currentX = gridLinePoint.x; currentX <= mGridBoundaryRect.right; currentX += distanceBetweenGridLines) {
            canvas.drawLine(gridLinePoint.x, gridLinePoint.y, gridLinePoint.x, mGridBoundaryRect.bottom, gridLinePaint);
            gridLinePoint.x += distanceBetweenGridLines;
        }


    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.screenWidth = w;
        this.screenHeight = h;

        // we have screen width, height, and grid size, lets draw grid,
        distanceBetweenGridLines = screenWidth / (mGridSize + 1); // one for one more edge, so the vertices count will remain as grid size
        gridLinePaint.setStrokeWidth(distanceBetweenGridLines / GameConstants.GRID_LINE_WIDTH_RATIO);
        mGridBoundaryRect = mGameUtils.getGridBoundaryRect(mGridSize, distanceBetweenGridLines);
    }

    private void init(){
        gridLinePaint = new Paint();
        gridLinePaint.setColor(GameConstants.PLAYER_ONE_COLOR);
        gridLinePaint.setStyle(Paint.Style.STROKE);
        mGridBoundaryRect = new Rect(0, 0, 0, 0);
        mGameUtils = new GameUtils();
    }


    public void drawGrid(int gridSize, int player){
        mGridSize = gridSize;
        switch (player){
            case GameConstants.PLAYER_ONE:
                gridLinePaint.setColor(GameConstants.PLAYER_ONE_COLOR);
                break;
            case GameConstants.PLAYER_TWO:
                gridLinePaint.setColor(GameConstants.PLAYER_TWO_COLOR);
                break;
            default:
                break;
        }
        invalidate();
    }
}
