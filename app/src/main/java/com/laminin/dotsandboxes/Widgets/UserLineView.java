package com.laminin.dotsandboxes.Widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.laminin.dotsandboxes.Beans.GameGridMapping;
import com.laminin.dotsandboxes.Utils.GameConstants;
import com.laminin.dotsandboxes.Utils.GameUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by franklin.michael on 30/07/16.
 */

public class UserLineView extends View {

    // screen height and width
    private int screenWidth;
    private int screenHeight;

    // player's draw line paint.
    private Paint playerOnePaint;
    private Paint playerTwoPaint;

    // player's rect fill paint
    private Paint playerOneRectPaint;
    private Paint playerTwoRectPaint;

    // current player
    private int mPlayer;

    // absolute source and destination points.
    private Point sourcePoint;
    private Point destinationPoint;

    // player drawn line width
    private int lineWidth;

    // drawing the lines on bitmap, then draw the bitmap on canvas to persit the previous drawings.
    private Bitmap canvasBitmap;

    // bitmap requires a canvas
    private Canvas mCanvas;

    // game utils.
    private GameUtils mGameUtils;

    // set contains the source and destination path and the user who drawn it.
    // sourcex#sourcey#destinationx#destinationy#player
    private Set<String> drawnPathSet;

    // player's scores.
    private int playerOneScore;
    private int playerTwoScore;

    // screen density
    private float screenDensity;
    // density scale
    private float densityMultiplicationFactor;
    // distance bw two lines.
    private int distanceBetweenGridLines;
    //  Player click buffer
    private int playerClickBuffer;

    private int mGridSize;

    private Rect mGridBoundaryRect;

    private GameGridMapping mGameGridMapping;

    public UserLineView(Context context) {
        super(context);
        init();
    }

    public UserLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UserLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){

//        screenDensity = getResources().getDisplayMetrics().density;
//        densityMultiplicationFactor = screenDensity / GameConstants.DEFAULT_SCREEN_DENSITY;

//        lineWidth = Math.round(8 * densityMultiplicationFactor);

//        distanceBetweenGridLines = Math.round(GameConstants.DISTANCE_BETWEEN_GRID_LINES * densityMultiplicationFactor);

        playerOnePaint = new Paint();
        playerOnePaint.setColor(GameConstants.PLAYER_ONE_GRID_COLOR);
        playerOnePaint.setStyle(Paint.Style.FILL);
        playerOnePaint.setStrokeWidth(lineWidth);

        playerTwoPaint = new Paint();
        playerTwoPaint.setColor(GameConstants.PLAYER_TWO_GRID_COLOR);
        playerTwoPaint.setStyle(Paint.Style.FILL);
        playerTwoPaint.setStrokeWidth(lineWidth);

        playerOneRectPaint = new Paint();
        playerOneRectPaint.setColor(GameConstants.PLAYER_ONE_COLOR);
        playerOneRectPaint.setStyle(Paint.Style.FILL);

        playerTwoRectPaint = new Paint();
        playerTwoRectPaint.setColor(GameConstants.PLAYER_TWO_COLOR);
        playerTwoRectPaint.setStyle(Paint.Style.FILL);

        mPlayer = GameConstants.PLAYER_ONE;
        sourcePoint = new Point(0, 0);
        destinationPoint = new Point(0, 0);

        mGameUtils = new GameUtils();

        drawnPathSet = new HashSet<>();

        playerTwoScore = 0;
        playerOneScore = 0;

//        playerClickBuffer = distanceBetweenGridLines / 4;

        mGridBoundaryRect = new Rect(0, 0, 0, 0);
    }

    /**
     * if a valid source and destination points are found
     * draw a line and check for should we draw a rect
     * @param playerSelectedPoint user selected point
     * @param player current player
     * @param gridXValuesSet all the x points
     * @param gridYValuesSet all the y points
     * @return next player.
     */
    public int draw(Point playerSelectedPoint, int player, LinkedHashSet<Integer> gridXValuesSet, LinkedHashSet<Integer>gridYValuesSet){
        mPlayer = player; // current player - line/rect will be drawn with this player's config

        sourcePoint.set(0, 0); // initially values are set to 0, 0
        destinationPoint.set(0, 0);

        ArrayList<Point> sourceAndDestinationPaths = mGameUtils.getSourceAndDestinationPoints(playerSelectedPoint, gridXValuesSet, gridYValuesSet, distanceBetweenGridLines, playerClickBuffer, mGridBoundaryRect.right, mGridBoundaryRect.bottom);

        // get absolute paths
        sourcePoint.set(sourceAndDestinationPaths.get(0).x, sourceAndDestinationPaths.get(0).y);
        destinationPoint.set(sourceAndDestinationPaths.get(1).x, sourceAndDestinationPaths.get(1).y);

        mGameGridMapping = new GameGridMapping();

        if(sourcePoint.x != 0 && sourcePoint.y != 0 && destinationPoint.x != 0 && destinationPoint.y != 0 && mGameUtils.isValidPath(sourcePoint, destinationPoint, distanceBetweenGridLines) && !mGameUtils.isDrawn(sourcePoint, destinationPoint, drawnPathSet)) { // we have the absolute paths and it is not drawn already
            mGameGridMapping = mGameUtils.getSourceAndDestinationRowColumn(sourcePoint, destinationPoint, gridXValuesSet, gridYValuesSet);
            switch (mPlayer){
                case GameConstants.PLAYER_ONE:
                    mCanvas.drawLine(sourcePoint.x, sourcePoint.y, destinationPoint.x, destinationPoint.y, playerOnePaint);
                    drawnPathSet.add(sourcePoint.x + "#" + sourcePoint.y + "#" + destinationPoint.x + "#" + destinationPoint.y);
                    ArrayList<Rect> fillBoxPlayerOne = mGameUtils.getRectIfValid(sourcePoint, destinationPoint, drawnPathSet, mGridBoundaryRect.right, mGridBoundaryRect.bottom, distanceBetweenGridLines, lineWidth / 2);
                    if(null != fillBoxPlayerOne){ // we can fill the rectangle now.
                        for(Rect rect : fillBoxPlayerOne) {
                            playerOneScore += 1;
                            mCanvas.drawRect(rect, playerOneRectPaint);
                        }
                        return player;
                    }
                    break;
                case GameConstants.PLAYER_TWO:
                    mCanvas.drawLine(sourcePoint.x, sourcePoint.y, destinationPoint.x, destinationPoint.y, playerTwoPaint);
                    drawnPathSet.add(sourcePoint.x + "#" + sourcePoint.y + "#" + destinationPoint.x + "#" + destinationPoint.y);
                    ArrayList<Rect> fillBoxPlayerTwo = mGameUtils.getRectIfValid(sourcePoint, destinationPoint, drawnPathSet,  mGridBoundaryRect.right, mGridBoundaryRect.bottom, distanceBetweenGridLines, lineWidth / 2);
                    if(null != fillBoxPlayerTwo){ // we can fill the rectangle now.
                        for(Rect rect : fillBoxPlayerTwo) {
                            playerTwoScore += 1;
                            mCanvas.drawRect(rect, playerTwoRectPaint);
                        }
                        return player;
                    }
                    break;
                default:
                    break;
            }
            // draw rect
            invalidate();
            return player == GameConstants.PLAYER_ONE ? GameConstants.PLAYER_TWO : GameConstants.PLAYER_ONE;
        } else{
            return player;
        }
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.screenWidth = w;
        this.screenHeight = h;

        distanceBetweenGridLines = screenWidth / (mGridSize + 1); // one for one more edge, so the vertices count will remain as grid size
        playerClickBuffer = distanceBetweenGridLines / 4;
        mGridBoundaryRect = mGameUtils.getGridBoundaryRect(mGridSize, distanceBetweenGridLines);
        // Adding additional 5 pixels for making enough space while drawing right, bottom lines
        mGridBoundaryRect.right = mGridBoundaryRect.right + 5;
        mGridBoundaryRect.bottom = mGridBoundaryRect.bottom + 5;
        lineWidth = distanceBetweenGridLines / GameConstants.USER_GRID_LINE_WIDTH_RATIO;
        playerOnePaint.setStrokeWidth(lineWidth);
        playerTwoPaint.setStrokeWidth(lineWidth);
        canvasBitmap = Bitmap.createBitmap(mGridBoundaryRect.right, mGridBoundaryRect.bottom, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(canvasBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(canvasBitmap, 0, 0, new Paint());
    }


    /**
     * return the current score for the given player.
     * @param player current player
     * @return current score
     */
    public int getScore(int player){
        switch (player){
            case GameConstants.PLAYER_ONE:
                return playerOneScore;
            case GameConstants.PLAYER_TWO:
                return playerTwoScore;
            default:
                return 0;
        }
    }

    /**
     * Method rest the userlineview
     */
    public void reset(){
        init();
        invalidate();
        if(screenWidth > 0 && screenHeight > 0) { // some times reset method might get called even before screen widht and screen height are being set
            canvasBitmap = Bitmap.createBitmap(screenWidth, screenHeight, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(canvasBitmap);
        }
    }

    public void setGridSize(int gridSize){
        mGridSize = gridSize;
        mGridBoundaryRect = mGameUtils.getGridBoundaryRect(mGridSize, distanceBetweenGridLines);
    }

    public GameGridMapping getGameGridMapping(){
        return mGameGridMapping;
    }
}
