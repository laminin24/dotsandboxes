package com.laminin.dotsandboxes.Utils;

import android.graphics.Color;

/**
 * Created by franklin.michael on 30/07/16.
 */

public class GameConstants {

    public static final int PLAYER_SYSTEM = 0; // TODO: implement system player
    public static final int PLAYER_ONE = 1; // constant for player one
    public static final int PLAYER_TWO = 2; // constant for player two


    // default dot circle color BLUE
    public static int DOT_CIRCLE_COLOR = Color.parseColor("#0000FF");

    public static int PLAYER_ONE_COLOR = Color.parseColor("#FF0000"); // player one color [grid color and rect fill color]
    public static int PLAYER_TWO_COLOR = Color.parseColor("#00FF00"); // player two color [grid color and rect fill color]

    public static int PLAYER_ONE_GRID_COLOR = Color.parseColor("#8B0000"); // player one color - draw line color
    public static int PLAYER_TWO_GRID_COLOR = Color.parseColor("#006400"); // player tow color - draw line color

    public static String PREFERENCE_NAME = "com.laminin.dotsandboxes.PREFERENCE_FILE_KEY"; // shared pref file name
    public static String IS_OLD_USER = "is_new_user"; // shared pref key
    public static String OLD_USER = "old_user"; // shared pref value
    public static String DEFAULT = "default"; // shared pref default string value

    // default distance between grid circles..
    public static int DISTANCE_BETWEEN_GRID_LINES = 200;

    // Radius of dot circle will be distance between grid lines / DOT_CIRCLE_RADIUS_RATIO
    public static int DOT_CIRCLE_RADIUS_RATIO = 10;

    // default user line width;
    public static int USER_GRID_LINE_WIDTH_RATIO = 10;


    // default grid line widths.
    public static int GRID_LINE_WIDTH_RATIO = 30;

    // game modes.
    public static final int SAME_DEVICE_MODE = 0; // default.
    public static final int DIFFERENT_DEVICE_MODE = 1;

    // Grid sizes
    public static final int THREE_INTO_THREE = 3;
    public static final int FOUR_INTO_FOUR = 4;
    public static final int FIVE_INTO_FIVE = 5;
    public static final int SIX_INTO_SIX = 6; // default.
    public static final int SEVEN_INTO_SEVEN = 7;

    public static final String GAME_CONFIG = "game_config";

    /*Certificate fingerprints:
    MD5:  72:53:4B:0C:39:94:39:A4:71:59:E0:87:94:53:35:5D
    SHA1: 3F:C0:F3:E4:5C:10:96:6D:CE:0D:20:E8:DC:88:69:87:DC:38:19:75
    SHA256: 38:66:41:7B:21:CC:E5:FC:F6:B3:A4:6C:A3:C2:13:3A:51:56:35:E3:8D:85:CC:0B:38:AE:89:70:2D:DE:28:8D
    Signature algorithm name: SHA1withRSA
    Version: 1*/

    public static final int GAME_MODE_FRAGMENT = 0;
    public static final int SELECT_USER_NAME_FRAGMENT = 1;
    public static final int SELECT_GRID_SIZE_FRAGMENT = 2;
    public static final int SELECT_USER_FRAGMENT = 3;

    public static final int DRAW_LINE_DIRECTION = -1;
    public static final int DRAW_LINE_HORIZONTAL = 0;
    public static final int DRAW_LINE_VERTICAL = 1;

}
