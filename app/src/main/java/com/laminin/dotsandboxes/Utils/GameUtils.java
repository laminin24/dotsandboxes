package com.laminin.dotsandboxes.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Rect;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.laminin.dotsandboxes.Beans.GameGridMapping;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by franklin.michael on 30/07/16.
 *
 * All the common methods should be written inside GameUtils.
 */

public class GameUtils {


    /**
     * When user tried to draw lines between two dots, we need an absolute paths to verify Whether it is a valid path or not.
     * @param point point to check.
     * @param pointsCollections are the valid x or y points of each dot.
     * @return return the absolute path if found else 0
     */
//    public int getAbsolutePosition(int point, Set<Integer> pointsCollections, int range){
//        for(int position : pointsCollections){
//            if(Math.abs(point - position) < range){ // we have found x position
//                return position;
//            }
//        }
//        return 0;
//    }

    /**
     * used for finding the given path is a valid path or not
     * Situation like diagonal, line between not adjacent paths will be taken care by this method.
     * @param source source path
     * @param destination destination path
     * @param distanceBetweenGrids distance between two dots.
     * @return boolean whether it is a right path or not.
     */
    public boolean isValidPath(Point source, Point destination, int  distanceBetweenGrids){
        return Math.hypot(destination.x - source.x, destination.y - source.y) == distanceBetweenGrids;
    }

    /**
     * Before drawing a line we need to check whether the path is already drawn or not
     * data in drawn path set are stored as sourcepoint.x#sourcepointy#destinationpointx#destinationpointy
     * @param sourcePoint source point
     * @param destinationPoint destination point
     * @param drawnPathSet all the drawn paths
     * @return boolean should draw a line or not
     */
    public boolean isDrawn(Point sourcePoint, Point destinationPoint, Set<String> drawnPathSet) {
        // source and destination are interchangeable.
        String sourceToDestination = sourcePoint.x + "#" + sourcePoint.y + "#" + destinationPoint.x + "#" + destinationPoint.y;
        String destinationToSource = destinationPoint.x + "#" + destinationPoint.y + "#" + sourcePoint.x + "#" + sourcePoint.y;
        return drawnPathSet.contains(sourceToDestination) || drawnPathSet.contains(destinationToSource);
    }

    /**
     * Method to check should we draw a square - does the player earns a point
     * @param sourcePoint source point of the drawn line
     * @param destinationPoint destination point of the drawn line
     * @param drawnPathSet all the available paths.
     * @param screenWidth total screen width;
     * @param screenHeight total screen height;
     * @param distanceBetweenGrids distance between grids
     * @param lineWidth line width
     * @return array list of rectangle, since more than one square is possible to draw at a time.
     */
    public ArrayList<Rect> getRectIfValid(Point sourcePoint, Point destinationPoint, Set<String> drawnPathSet, int screenWidth, int screenHeight, int distanceBetweenGrids, int lineWidth) {

        // A temp point to interchange source and destination in case of line drawn from destination to source.
        // ie source is greater than destination
        Point point = new Point();

        // more than one rect can be drawn for one line
        ArrayList <Rect> rectangleCollections = new ArrayList<>();

        if(sourcePoint.y == destinationPoint.y){ // Line is drawn horizontally

            // user has drawn the line horizontally
            // we need to check both upper part and down part of the line to check whether we can draw  rectangle / rectangles.

            // interchange source and destination
            if(sourcePoint.x > destinationPoint.x){
                point.set(sourcePoint.x, sourcePoint.y);
                sourcePoint.set(destinationPoint.x, destinationPoint.y);
                destinationPoint.set(point.x, point.y);
            }

            String leftPathSourceToDestination = "";
            String leftPathDestinationToSource = "";
            String oppositePathSourceToDestination = "";
            String oppositePathDestinationToSource = "";
            String rightPathSourceToDestination = "";
            String rightPathDestinationToSource = "";

            // check upper part
            if(sourcePoint.y > distanceBetweenGrids){ // we need to check upper part for drawing rectangle
                leftPathSourceToDestination = sourcePoint.x + "#" + sourcePoint.y + "#" + sourcePoint.x + "#" + (sourcePoint.y - distanceBetweenGrids);
                leftPathDestinationToSource = sourcePoint.x + "#" + (sourcePoint.y - distanceBetweenGrids) + "#" +  sourcePoint.x + "#" + sourcePoint.y;

                rightPathSourceToDestination = destinationPoint.x + "#" + destinationPoint.y + "#" + destinationPoint.x + "#" + (destinationPoint.y - distanceBetweenGrids);
                rightPathDestinationToSource = destinationPoint.x + "#" + (destinationPoint.y - distanceBetweenGrids) + "#" + destinationPoint.x + "#" + destinationPoint.y;

                oppositePathSourceToDestination = sourcePoint.x + "#" + (sourcePoint.y - distanceBetweenGrids) + "#" + destinationPoint.x + "#" + (destinationPoint.y - distanceBetweenGrids);
                oppositePathDestinationToSource = destinationPoint.x + "#" + (destinationPoint.y - distanceBetweenGrids) + "#" + sourcePoint.x + "#" + (sourcePoint.y - distanceBetweenGrids);

                if((drawnPathSet.contains(leftPathDestinationToSource) || drawnPathSet.contains(leftPathSourceToDestination)) &&
                        (drawnPathSet.contains(rightPathDestinationToSource) || drawnPathSet.contains(rightPathSourceToDestination)) &&
                        (drawnPathSet.contains(oppositePathDestinationToSource) || drawnPathSet.contains(oppositePathSourceToDestination))){

                    // line width adjustments are for making the rectangle lines inside the user grid lines.
                    // the rectangle wont overdraw the grid.
                    // Grid borders will be the same as user drawn color.
                    rectangleCollections.add(new Rect(sourcePoint.x + lineWidth, sourcePoint.y + lineWidth - distanceBetweenGrids, destinationPoint.x - lineWidth, destinationPoint.y - lineWidth));
                }
            }

            // check down part
            if(sourcePoint.y + distanceBetweenGrids <= screenHeight){ // we need to check down part for drawing rectangle
                leftPathSourceToDestination = sourcePoint.x + "#" + sourcePoint.y + "#" + sourcePoint.x + "#" + (sourcePoint.y + distanceBetweenGrids);
                leftPathDestinationToSource = sourcePoint.x + "#" + (sourcePoint.y + distanceBetweenGrids) + "#" + sourcePoint.x + "#" + sourcePoint.y;

                rightPathSourceToDestination = destinationPoint.x + "#" + destinationPoint.y + "#" + destinationPoint.x + "#" + (destinationPoint.y + distanceBetweenGrids);
                rightPathDestinationToSource = destinationPoint.x + "#" + (destinationPoint.y + distanceBetweenGrids) + "#" + destinationPoint.x + "#" + destinationPoint.y;

                oppositePathSourceToDestination = sourcePoint.x + "#" + (sourcePoint.y + distanceBetweenGrids) + "#" + destinationPoint.x + "#" + (destinationPoint.y + distanceBetweenGrids);
                oppositePathDestinationToSource = destinationPoint.x + "#" + (destinationPoint.y + distanceBetweenGrids) + "#" + sourcePoint.x + "#" + (sourcePoint.y + distanceBetweenGrids);

                if((drawnPathSet.contains(leftPathDestinationToSource) || drawnPathSet.contains(leftPathSourceToDestination)) &&
                        (drawnPathSet.contains(rightPathDestinationToSource) || drawnPathSet.contains(rightPathSourceToDestination)) &&
                        (drawnPathSet.contains(oppositePathDestinationToSource) || drawnPathSet.contains(oppositePathSourceToDestination))){

                    rectangleCollections.add(new Rect(sourcePoint.x + lineWidth, sourcePoint.y - lineWidth + distanceBetweenGrids, destinationPoint.x - lineWidth, destinationPoint.y + lineWidth));
                }
            }
        }else { // Line is drawn vertically.

            String upPathSourceToDestination = "";
            String upPathDestinationToSource = "";
            String bottomPathSourceToDestination = "";
            String bottomPathDestinationToSource = "";
            String oppositePathSourceToDestination = "";
            String oppositePathDestinationToSource = "";


            if(sourcePoint.y > destinationPoint.y){
                point.set(sourcePoint.x, sourcePoint.y);
                sourcePoint.set(destinationPoint.x, destinationPoint.y);
                destinationPoint.set(point.x, point.y);
            }

            // Check left side
            if(sourcePoint.x > distanceBetweenGrids){
                upPathSourceToDestination = sourcePoint.x + "#" + sourcePoint.y + "#" + (sourcePoint.x - distanceBetweenGrids) + "#" + sourcePoint.y;
                upPathDestinationToSource = (sourcePoint.x - distanceBetweenGrids) + "#" + sourcePoint.y + "#" + sourcePoint.x + "#" + sourcePoint.y;
                bottomPathSourceToDestination = destinationPoint.x + "#" + destinationPoint.y + "#" + (destinationPoint.x - distanceBetweenGrids) + "#" + destinationPoint.y;
                bottomPathDestinationToSource = (destinationPoint.x - distanceBetweenGrids) + "#" + destinationPoint.y + "#" + destinationPoint.x + "#" + destinationPoint.y;
                oppositePathSourceToDestination = (sourcePoint.x - distanceBetweenGrids) + "#" + sourcePoint.y + "#" + (destinationPoint.x - distanceBetweenGrids) + "#" + destinationPoint.y;
                oppositePathDestinationToSource = (destinationPoint.x - distanceBetweenGrids) + "#" + destinationPoint.y + "#" + (sourcePoint.x - distanceBetweenGrids) + "#" + sourcePoint.y;

                if((drawnPathSet.contains(upPathDestinationToSource) || drawnPathSet.contains(upPathSourceToDestination)) &&
                        (drawnPathSet.contains(bottomPathDestinationToSource) || drawnPathSet.contains(bottomPathSourceToDestination)) &&
                        (drawnPathSet.contains(oppositePathDestinationToSource) || drawnPathSet.contains(oppositePathSourceToDestination))){

                    rectangleCollections.add(new Rect(sourcePoint.x + lineWidth - distanceBetweenGrids, sourcePoint.y + lineWidth, destinationPoint.x - lineWidth, destinationPoint.y - lineWidth));
                }
            }

            // check right side
            if(sourcePoint.x + distanceBetweenGrids <= screenWidth){
                upPathSourceToDestination = sourcePoint.x + "#" + sourcePoint.y + "#" + (sourcePoint.x + distanceBetweenGrids) + "#" + sourcePoint.y;
                upPathDestinationToSource = (sourcePoint.x + distanceBetweenGrids) + "#" + sourcePoint.y + "#" + sourcePoint.x + "#" + sourcePoint.y;
                bottomPathSourceToDestination = destinationPoint.x + "#" + destinationPoint.y + "#" + (destinationPoint.x + distanceBetweenGrids) + "#" + destinationPoint.y;
                bottomPathDestinationToSource = (destinationPoint.x + distanceBetweenGrids) + "#" + destinationPoint.y + "#" + destinationPoint.x + "#" + destinationPoint.y;
                oppositePathSourceToDestination = (sourcePoint.x + distanceBetweenGrids) + "#" + sourcePoint.y + "#" + (destinationPoint.x + distanceBetweenGrids) + "#" + destinationPoint.y;
                oppositePathDestinationToSource = (destinationPoint.x + distanceBetweenGrids) + "#" + destinationPoint.y + "#" + (sourcePoint.x + distanceBetweenGrids) + "#" + sourcePoint.y;

                if((drawnPathSet.contains(upPathDestinationToSource) || drawnPathSet.contains(upPathSourceToDestination)) &&
                        (drawnPathSet.contains(bottomPathDestinationToSource) || drawnPathSet.contains(bottomPathSourceToDestination)) &&
                        (drawnPathSet.contains(oppositePathDestinationToSource) || drawnPathSet.contains(oppositePathSourceToDestination))){

                    rectangleCollections.add(new Rect(sourcePoint.x + lineWidth, sourcePoint.y + lineWidth, destinationPoint.x - lineWidth + distanceBetweenGrids, destinationPoint.y - lineWidth));

                }
            }
        }

        return rectangleCollections.size() > 0 ? rectangleCollections : null ;
    }


    /**
     * Write data to shard pref
     * @param context current activity context
     * @param preferenceName shard pref name.
     * @param key key
     * @param value value
     * @return boolean commit status
     */
    public boolean writeToPreference(Context context, String preferenceName, String key, String value){
        SharedPreferences sharedPref = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    /**
     * read data from shared pref
     * @param context current context
     * @param preferenceName shard pref name.
     * @param key key
     * @param defaultValue default return value
     * @return value
     */
    public String readFromPreference(Context context, String preferenceName, String key, String defaultValue){
        SharedPreferences sharedPref = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        return sharedPref.getString(key, defaultValue);
    }

    public ArrayList<Point> getSourceAndDestinationPoints(Point playerSelectedPoint, Set<Integer> gridXValuesSet, Set<Integer> gridYValuesSet, int distanceBetweenGrids, int playerClickBuffer, int screenWidth, int screenHeight) {
        // should draw vertical check
        Rect playerSelectedVerticalRect = new Rect(Math.max(0, playerSelectedPoint.x - playerClickBuffer), Math.max(0, playerSelectedPoint.y - distanceBetweenGrids), Math.min(playerSelectedPoint.x + playerClickBuffer, screenWidth), Math.min(playerSelectedPoint.y + distanceBetweenGrids, screenHeight));
        // Horizontal rect check
        Rect playerSelectedHorizontalRect = new Rect(Math.max(playerSelectedPoint.x - distanceBetweenGrids, 0), Math.max(playerSelectedPoint.y - playerClickBuffer, 0), Math.min(playerSelectedPoint.x + distanceBetweenGrids, screenWidth), Math.min(playerSelectedPoint.y + playerClickBuffer, screenHeight));

        Point sourcePoint = new Point(0, 0);
        Point destinationPoint = new Point(0, 0);
        for(int pointY : gridYValuesSet){
            for(int pointX : gridXValuesSet){
                if(playerSelectedHorizontalRect.contains(pointX, pointY, pointX + distanceBetweenGrids, pointY)){ // horizontal point
                    sourcePoint.set(pointX, pointY);
                    destinationPoint.set(pointX + distanceBetweenGrids, pointY);
                }else if(playerSelectedVerticalRect.contains(pointX, pointY, pointX, pointY + distanceBetweenGrids)){ //vertical check
                    sourcePoint.set(pointX, pointY);
                    destinationPoint.set(pointX, pointY + distanceBetweenGrids);
                }
            }
        }

        ArrayList<Point> sourceAndDestinationPointsList = new ArrayList<>();
        sourceAndDestinationPointsList.add(sourcePoint);
        sourceAndDestinationPointsList.add(destinationPoint);

        return sourceAndDestinationPointsList;
    }

    public Rect getGridBoundaryRect(int gridSize, int distanceBetweenGridLines) {
        int xMargin = distanceBetweenGridLines / 2; //for padding around. - This will be the starting point of x
        // vertical grid adjustment
        int oddFactor = gridSize % 2 != 0 ? distanceBetweenGridLines / 2 : 0;
        int pointY = distanceBetweenGridLines * (gridSize / 2) + oddFactor;
        return new Rect(xMargin, pointY, xMargin + (distanceBetweenGridLines * gridSize), pointY + (distanceBetweenGridLines * gridSize));
    }

    /**
     * for the given source and destination points we find the row and column number for teh source and destination.
     * We will need this because Player 1 screen size and player 2 screen size may not be same.
     * @param sourcePoint
     * @param destinationPoint
     * @param gridXValuesSet
     * @param gridYValuesSet
     * @return
     */
    public GameGridMapping getSourceAndDestinationRowColumn(Point sourcePoint, Point destinationPoint, LinkedHashSet<Integer> gridXValuesSet, LinkedHashSet<Integer> gridYValuesSet) {
        GameGridMapping gameGridMapping = new GameGridMapping();
        ArrayList<Integer> gridXList = new ArrayList<>(gridXValuesSet);
        for(int i = 0; i < gridXList.size(); i++){
            if(sourcePoint.x == gridXList.get(i)){
                gameGridMapping.setSourceRowNumber(i);
            }
            if(destinationPoint.x == gridXList.get(i)){
                gameGridMapping.setDestinationRowNumber(i);
            }
        }

        ArrayList<Integer> gridYList = new ArrayList<>(gridYValuesSet);
        for(int i = 0; i < gridYList.size(); i++){
            if(sourcePoint.y == gridYList.get(i)){
                gameGridMapping.setSourceColumnNumber(i);
            }
            if(destinationPoint.y == gridYList.get(i)){
                gameGridMapping.setDestinationColumnNumber(i);
            }
        }

        return gameGridMapping;
    }

    /**
     * Once we get the source and destination row and column we can find the actual coordinates using this method.
     * @param receivedGameGridMapping
     * @param gridXValuesSet
     * @param gridYValuesSet
     * @return
     */
    public Point getPointFromGridMapping(GameGridMapping receivedGameGridMapping, LinkedHashSet<Integer> gridXValuesSet, LinkedHashSet<Integer> gridYValuesSet) {

        Point sourcePoint = new Point();
        Point destinationPoint = new Point();
        Point midPoint = new Point();

        ArrayList<Integer> gridXList = new ArrayList<>(gridXValuesSet);
        ArrayList<Integer> gridYList = new ArrayList<>(gridYValuesSet);

        sourcePoint.x = gridXList.get(receivedGameGridMapping.getSourceRowNumber());
        sourcePoint.y = gridYList.get(receivedGameGridMapping.getSourceColumnNumber());

        destinationPoint.x = gridXList.get(receivedGameGridMapping.getDestinationRowNumber());
        destinationPoint.y = gridYList.get(receivedGameGridMapping.getDestinationColumnNumber());

        midPoint.x = (sourcePoint.x + destinationPoint.x) / 2;
        midPoint.y = (sourcePoint.y + destinationPoint.y) / 2;

        return midPoint;
    }
}
