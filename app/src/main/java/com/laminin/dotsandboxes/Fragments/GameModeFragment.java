package com.laminin.dotsandboxes.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.laminin.dotsandboxes.Beans.GameConfig;
import com.laminin.dotsandboxes.Interfaces.OnFragmentInteractionListener;
import com.laminin.dotsandboxes.R;
import com.laminin.dotsandboxes.Utils.GameConstants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GameModeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GameModeFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = GameModeFragment.class.getName();

    // TODO: Rename and change types of parameters
    private GameConfig mGameConfig;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public GameModeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param gameConfig Game configuration object.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GameModeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GameModeFragment newInstance(GameConfig gameConfig, String param2) {
        GameModeFragment fragment = new GameModeFragment();
        Bundle args = new Bundle();
        args.putParcelable(GameConstants.GAME_CONFIG, gameConfig);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGameConfig = getArguments().getParcelable(GameConstants.GAME_CONFIG);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_mode, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ImageButton oneDeviceImageButton = getActivity().findViewById(R.id.one_device_mode_image_button);
        oneDeviceImageButton.setColorFilter(getResources().getColor(R.color.colorClickableText));
        oneDeviceImageButton.setOnClickListener(this);
        ImageButton differentDevicesImageButton = getActivity().findViewById(R.id.different_devices_mode_image_button);
        differentDevicesImageButton.setOnClickListener(this);
        differentDevicesImageButton.setColorFilter(getResources().getColor(R.color.colorClickableText));

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.one_device_mode_image_button:
                mGameConfig.setGameMode(GameConstants.SAME_DEVICE_MODE);
                mListener.userClicked(mGameConfig, false);
                break;
            case R.id.different_devices_mode_image_button:
                mGameConfig.setGameMode(GameConstants.DIFFERENT_DEVICE_MODE);
                mListener.userClicked(mGameConfig, false);
                break;
            default:
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
