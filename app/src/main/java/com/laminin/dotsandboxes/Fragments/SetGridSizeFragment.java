package com.laminin.dotsandboxes.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.laminin.dotsandboxes.Beans.GameConfig;
import com.laminin.dotsandboxes.Interfaces.OnFragmentInteractionListener;
import com.laminin.dotsandboxes.R;
import com.laminin.dotsandboxes.Utils.GameConstants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SetGridSizeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SetGridSizeFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = SetGridSizeFragment.class.getName();

    // TODO: Rename and change types of parameters
    private GameConfig mGameConfig;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private int clickableId[] = {R.id.text_view_3x3, R.id.text_view_4x4, R.id.text_view_5x5, R.id.text_view_6x6, R.id.text_view_7x7, R.id.text_view_play_online};

    public SetGridSizeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param gameConfig Game config 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SetGridSizeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SetGridSizeFragment newInstance(GameConfig gameConfig, String param2) {
        SetGridSizeFragment fragment = new SetGridSizeFragment();
        Bundle args = new Bundle();
        args.putParcelable(GameConstants.GAME_CONFIG, gameConfig);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGameConfig = getArguments().getParcelable(GameConstants.GAME_CONFIG);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_set_grid_size, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        for(int id : clickableId){
            view.findViewById(id).setOnClickListener(this);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.text_view_3x3:
                mGameConfig.setGameMode(GameConstants.SAME_DEVICE_MODE);
                mGameConfig.setGridSize(GameConstants.THREE_INTO_THREE);
                mListener.userClicked(mGameConfig, true);
                break;
            case R.id.text_view_4x4:
                mGameConfig.setGameMode(GameConstants.SAME_DEVICE_MODE);
                mGameConfig.setGridSize(GameConstants.FOUR_INTO_FOUR);
                mListener.userClicked(mGameConfig, true);
                break;
            case R.id.text_view_5x5:
                mGameConfig.setGameMode(GameConstants.SAME_DEVICE_MODE);
                mGameConfig.setGridSize(GameConstants.FIVE_INTO_FIVE);
                mListener.userClicked(mGameConfig, true);
                break;
            case R.id.text_view_6x6:
                mGameConfig.setGameMode(GameConstants.SAME_DEVICE_MODE);
                mGameConfig.setGridSize(GameConstants.SIX_INTO_SIX);
                mListener.userClicked(mGameConfig, true);
                break;
            case R.id.text_view_7x7:
                mGameConfig.setGameMode(GameConstants.SAME_DEVICE_MODE);
                mGameConfig.setGridSize(GameConstants.SEVEN_INTO_SEVEN);
                mListener.userClicked(mGameConfig, true);
                break;
            case R.id.text_view_play_online:
                mGameConfig.setGameMode(GameConstants.DIFFERENT_DEVICE_MODE);
                mGameConfig.setGridSize(GameConstants.SIX_INTO_SIX);
                mListener.userClicked(mGameConfig, false);
                break;
            default:
//                R.id.seven_into_seven_image_button:
//                mListener.userClicked(mGameConfig, true);
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
