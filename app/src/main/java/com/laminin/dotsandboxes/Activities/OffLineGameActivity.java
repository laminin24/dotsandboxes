package com.laminin.dotsandboxes.Activities;

import android.content.DialogInterface;
import android.graphics.Point;
import android.provider.Settings;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.laminin.dotsandboxes.Beans.GameConfig;
import com.laminin.dotsandboxes.R;
import com.laminin.dotsandboxes.Utils.GameConstants;
import com.laminin.dotsandboxes.Widgets.GridLineView;
import com.laminin.dotsandboxes.Widgets.GridView;
import com.laminin.dotsandboxes.Widgets.UserLineView;

public class OffLineGameActivity extends AppCompatActivity implements View.OnTouchListener{

    // Game config
    private GameConfig mGameConfig;
    // Grid View - dot circles view
    private GridView mGridView;
    // Grid line view
    private GridLineView mGridLineView;
    // User line view
    private UserLineView mUserLineView;
    // player
    private int mPlayer;
    // Point action up point;
    private Point actionUpPoint;

    // Fire base interstitial ad
    private InterstitialAd mInterstitialAd;


    // Fire base analytics
    private FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mGameConfig = getIntent().getParcelableExtra(GameConstants.GAME_CONFIG);
        if(null == mGameConfig) mGameConfig = new GameConfig();

        mGridView = (GridView) findViewById(R.id.grid_view);
        mGridView.drawGrid(mGameConfig.getGridSize());
        mGridView.setOnTouchListener(this);

        mGridLineView = (GridLineView) findViewById(R.id.grid_line_view);
        mPlayer = GameConstants.PLAYER_ONE; // TODO make the player random.
        mGridLineView.drawGrid(mGameConfig.getGridSize(), mPlayer);

        mUserLineView = (UserLineView) findViewById(R.id.user_line_view);
        mUserLineView.setGridSize(mGameConfig.getGridSize());
        actionUpPoint = new Point(0, 0);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.add_unit_id));

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                showGameDialog(mUserLineView.getScore(GameConstants.PLAYER_ONE), mUserLineView.getScore(GameConstants.PLAYER_TWO));
            }
        });

        startNewGame();

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = MotionEventCompat.getActionMasked(motionEvent);
        switch(action) {
            case (MotionEvent.ACTION_DOWN):
                break;
            case (MotionEvent.ACTION_MOVE):
                break;
            case (MotionEvent.ACTION_UP):
                actionUpPoint.x = Math.round(motionEvent.getX());
                actionUpPoint.y = Math.round(motionEvent.getY());
                    mPlayer = mUserLineView.draw(actionUpPoint, mPlayer, mGridView.getGridXValesSet(), mGridView.getGridYValuesSet());
                    mGridLineView.drawGrid(mGameConfig.getGridSize(), mPlayer);

                    int playerOneScore = mUserLineView.getScore(GameConstants.PLAYER_ONE);
                    int playerTwoScore = mUserLineView.getScore(GameConstants.PLAYER_TWO);

                    if(playerOneScore + playerTwoScore == mGridView.getTotalRectCount()){ // Game over
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                        } else {
                            showGameDialog(playerOneScore, playerTwoScore);
                        }
                    }
                actionUpPoint.set(0, 0);
                break;
            case (MotionEvent.ACTION_CANCEL):
                break;
            case (MotionEvent.ACTION_OUTSIDE):
                break;
            default:
                break;
        }

        return true;
    }

    private void showGameDialog(int playerOneScore, int playerTwoScore){
        AlertDialog.Builder gameOverDialogBuilder = new AlertDialog.Builder(this);
        View gamerOverDialogView = getLayoutInflater().inflate(R.layout.game_over_alert, null);
        gameOverDialogBuilder.setView(gamerOverDialogView);
        gameOverDialogBuilder.setCancelable(false);

        TextView textViewMessage = (TextView) gamerOverDialogView.findViewById(R.id.text_view_game_over);
        textViewMessage.setText("Game Over!");

        TextView textViewPlayerOneScore = (TextView) gamerOverDialogView.findViewById(R.id.text_view_player_one_score);
        textViewPlayerOneScore.setText(""+ playerOneScore);

        TextView textViewPlayerTwoScore = (TextView) gamerOverDialogView.findViewById(R.id.text_view_player_two_score);
        textViewPlayerTwoScore.setText(""+ playerTwoScore);

        gameOverDialogBuilder.setPositiveButton("New Game", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startNewGame();
            }
        });

        gameOverDialogBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        gameOverDialogBuilder.show();
    }

    private void startNewGame(){

        actionUpPoint = new Point(0, 0);
        mPlayer = GameConstants.PLAYER_ONE;

        mUserLineView.reset();
        mUserLineView.setGridSize(mGameConfig.getGridSize());
        mGridView.drawGrid(mGameConfig.getGridSize());
        mGridLineView.drawGrid(mGameConfig.getGridSize(), mPlayer);

        requestNewInterstitial();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(Settings.Secure.ANDROID_ID)
                .build();

        mInterstitialAd.loadAd(adRequest);
    }
}
