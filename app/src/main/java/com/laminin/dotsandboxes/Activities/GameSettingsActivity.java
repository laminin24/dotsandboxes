package com.laminin.dotsandboxes.Activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.laminin.dotsandboxes.Beans.GameConfig;
import com.laminin.dotsandboxes.Fragments.GameModeFragment;
import com.laminin.dotsandboxes.Fragments.SetGridSizeFragment;
import com.laminin.dotsandboxes.Interfaces.OnFragmentInteractionListener;
import com.laminin.dotsandboxes.R;
import com.laminin.dotsandboxes.Utils.GameConstants;

public class GameSettingsActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    //Game config;
    GameConfig mGameConfig;
    String currentLoadedFragment = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_settings);
        mGameConfig = new GameConfig();
        addFragment(GameModeFragment.newInstance(mGameConfig, GameModeFragment.TAG), GameModeFragment.TAG);
    }

    @Override
    public void userClicked(GameConfig gameConfig, boolean shouldLaunchGame) {
        if(gameConfig.getGameMode() == GameConstants.SAME_DEVICE_MODE){
            if(shouldLaunchGame){
                Intent intent = new Intent(this, OffLineGameActivity.class);
                intent.putExtra(GameConstants.GAME_CONFIG, gameConfig);
                startActivity(intent);
            } else {
                replaceFragment(SetGridSizeFragment.newInstance(mGameConfig, SetGridSizeFragment.TAG), SetGridSizeFragment.TAG);
            }
        } else {
            Intent intent = new Intent(this, OnlineGameActivity.class);
            intent.putExtra(GameConstants.GAME_CONFIG, gameConfig);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if(currentLoadedFragment.contentEquals(SetGridSizeFragment.TAG)){
            replaceFragment(GameModeFragment.newInstance(mGameConfig, GameModeFragment.TAG), GameModeFragment.TAG);
        } else {
            super.onBackPressed();
        }
    }

    private void addFragment(Fragment fragment, String tag) {
        currentLoadedFragment = tag;
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
    }

    private void replaceFragment(Fragment fragment, String tag) {
        currentLoadedFragment = tag;
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }
}
