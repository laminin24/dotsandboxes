package com.laminin.dotsandboxes.Interfaces;

import com.laminin.dotsandboxes.Beans.GameConfig;

/**
 * Created by franklin.michael on 08/08/16.
 */

public interface OnFragmentInteractionListener {
    void userClicked(GameConfig gameConfig, boolean shouldLaunchGame);
}
